#!/bin/bash

#wget -O - https://gitlab.com/JonW/PCFastBuildScripts/raw/master/Armbian-NanoPi-BootstrapBuild.sh | bash
# this is used to put dependencies in place for PCFastRelease

apt-get -y -qq install git
git clone --depth 1 https://github.com/igorpecovnik/lib
cp -v lib/compile.sh .

USERPATCHES="/root/userpatches"
OVERLAY="/root/userpatches/overlay"

mkdir $USERPATCHES
mkdir $OVERLAY

curl -o $USERPATCHES/customize-image.sh https://gitlab.com/JonW/PCFastBuildScripts/raw/master/Armbian-NanoPi-customize-image.sh
curl -o $USERPATCHES/lib.config https://gitlab.com/JonW/PCFastBuildScripts/raw/master/Armbian-NanoPi-lib.config
curl -o $OVERLAY/FirstBoot.sh https://gitlab.com/JonW/PCFastBuildScripts/raw/master/Armbian-NanoPi-FirstBoot.sh

DIRECTORY="$OVERLAY/PCFastUpdater"
mkdir $DIRECTORY
git -C $DIRECTORY clone --recursive https://gitlab.com/JonW/PCFastUpdater.git .
touch $DIRECTORY/NanoPi

DIRECTORY="$OVERLAY/PCFastRelease"
mkdir $DIRECTORY
git -C $DIRECTORY clone --recursive https://gitlab.com/JonW/PCFastRelease-NanoPi.git .

DIRECTORY="$OVERLAY/PCFastManage"
mkdir $DIRECTORY
git -C $DIRECTORY clone --recursive https://gitlab.com/JonW/PCFastManage.git .

bash /root/compile.sh BRANCH=default BOARD=nanopineo KERNEL_ONLY=no PROGRESS_DISPLAY=plain RELEASE=xenial BUILD_DESKTOP=no