#!/bin/bash

# arguments: $RELEASE $FAMILY $BOARD $BUILD_DESKTOP
#
# This is the image customization script
#
# NOTE: It is copied to /tmp directory inside the image
# and executed there inside chroot environment
# so don't reference any files that are not already installed

RELEASE=$1
FAMILY=$2
BOARD=$3
BUILD_DESKTOP=$4

case $RELEASE in
	wheezy)
	# your code here
	;;
	jessie)
	# your code here
	;;
	trusty)
	# your code here
	;;
	xenial)
		echo "Starting PCFastRelease OSBuild 1.1.4"
		sleep 4

		echo "remove login create account"
			rm /root/.not_logged_in_yet

		echo "Make root user login with my public ssh key"
			mkdir ~/.ssh
			chmod 700 ~/.ssh
			curl -L https://gitlab.com/JonW.keys  >> ~/.ssh/authorized_keys

		echo "Change SSH port"
			sed -i 's/Port 22/Port 2200/g' /etc/ssh/sshd_config
			sed -i 's/#PasswordAuthentication yes/PasswordAuthentication no/g' /etc/ssh/sshd_config
			sed -i 's/UsePAM yes/UsePAM no/g' /etc/ssh/sshd_config
		
		echo "Add temp"
			mkdir -p -m775 /etc/armbianmonitor/datasources/
			ln -fs /sys/devices/virtual/thermal/thermal_zone1/temp /etc/armbianmonitor/datasources/soctemp
		
		echo "Set the timezone"
			rm -rf /etc/localtime
			ln -s /usr/share/zoneinfo/Australia/Melbourne /etc/localtime

		#echo "Add Required apps"
			#apt-get update
			#apt-get --yes upgrade
			#apt-get --yes install ufw git python3-dev python3-pip python3-setuptools python3-wheel

		echo "Install python SPI-Py"
			cd ~ 
			git clone https://github.com/lthiery/SPI-Py.git
			cd SPI-Py
			python3 setup.py install
			cd ~
			rm -rf SPI-Py

		echo "install gpio"
			pip3 install gpio

		echo "install python netifaces"
			pip3 install netifaces

		
		APPSTORE="/usr/local/bin"
		echo "Download PCFastUpdater"
			SOURCE="/tmp/overlay/PCFastUpdater/"
			DIRECTORY="/usr/local/bin/PCFastUpdater"
			
			
			rm -rf $DIRECTORY
			cp -rv $SOURCE $APPSTORE
			chmod 710 $DIRECTORY
			
			cp -v $DIRECTORY/PCFastUpdater.service /lib/systemd/system/PCFastUpdater.service
			cp -v $DIRECTORY/PCFastUpdater.timer /lib/systemd/system/PCFastUpdater.timer
			
			touch $DIRECTORY/NanoPi
		
			systemctl daemon-reload
			systemctl enable PCFastUpdater.timer
			systemctl start PCFastUpdater.timer
			
		echo "Install PCFastRelease"
			SOURCE="/tmp/overlay/PCFastRelease/"
			DIRECTORY="/usr/local/bin/PCFastRelease"
			
			rm -rf $DIRECTORY
			cp -rv $SOURCE $APPSTORE
			chmod 710 $DIRECTORY

			cp -v $DIRECTORY/PCFastRelease.service /lib/systemd/system/PCFastRelease.service

			systemctl daemon-reload
			systemctl enable PCFastRelease.service

		echo "Install PCFastManage"
			SOURCE="/tmp/overlay/PCFastManage/"
			DIRECTORY="/usr/local/bin/PCFastManage"
			
			rm -rf $DIRECTORY
			cp -rv $SOURCE $APPSTORE
			chmod 710 $DIRECTORY
			
			cp -v $DIRECTORY/YonTech.xyz.CA.crt /usr/local/share/ca-certificates/YonTech.xyz.CA.crt
			update-ca-certificates
			
			cp -v $DIRECTORY/PCFastManage.service /lib/systemd/system/PCFastManage.service
			cp -v $DIRECTORY/PCFastManage.timer /lib/systemd/system/PCFastManage.timer

			systemctl daemon-reload
			systemctl enable PCFastManage.timer

		echo "Setup Log"
			curl -o /etc/papertrail-bundle.pem https://papertrailapp.com/tools/papertrail-bundle.pem

			echo '$DefaultNetstreamDriverCAFile /etc/papertrail-bundle.pem # trust these CAs'>> /etc/rsyslog.conf
			echo '$ActionSendStreamDriver gtls # use gtls netstream driver'>> /etc/rsyslog.conf
			echo '$ActionSendStreamDriverMode 1 # require TLS'>> /etc/rsyslog.conf
			echo '$ActionSendStreamDriverAuthMode x509/name # authenticate by hostname'>> /etc/rsyslog.conf
			echo '$ActionSendStreamDriverPermittedPeer *.papertrailapp.com'>> /etc/rsyslog.conf
			echo '*.*          @logs4.papertrailapp.com:21012' >> /etc/rsyslog.conf

		#echo "Setup PyMonitor"
			#armbianmonitor -r
		
		echo "Install FirstBoot"
			SOURCE="/tmp/overlay/FirstBoot.sh"
			DESTINATION="/etc/init.d/FirstBoot.sh"
			
			cp -v $SOURCE $DESTINATION
			chmod +x $DESTINATION
			update-rc.d FirstBoot.sh defaults
		
	;;
esac
