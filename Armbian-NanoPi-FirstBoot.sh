#!/bin/bash
### BEGIN INIT INFO
# Provides:          RegenSSH
# Required-Start:    $remote_fs $syslog
# Required-Stop:     $remote_fs $syslog
# Default-Start:     2 3 4 5
# Default-Stop:      0 1 6
# Short-Description: RegenSSH removes old SSH keys and creates new ones.
# Description:       RegenSSH removes old SSH keys and creates new ones.
### END INIT INFO

#Configure Firewall
ufw default deny incoming
ufw default allow outgoing
	
ufw allow 2200/tcp
ufw allow 4800/tcp
ufw allow 8888/tcp

#enable the rules and reboot
ufw --force enable;

# Delete me
rm $0